import React from "react";
import { Container, Row, Col, Form, Button, Tabs, Tab, Modal } from "react-bootstrap";
// import { library } from '@fortawesome/fontawesome-svg-core';
// import { fab } from '@fortawesome/free-brands-svg-icons';
// import { faCheckSquare, faCoffee } from '@fortawesome/free-solid-svg-icons';
 
// library.add(fab, faCheckSquare, faCoffee);

import Header from "./components/header";
import Body from "./components/body";
import Footer from "./components/footer";
import './App.scss';

class App extends React.Component {

  render() {
    return(
      <Container fluid className="fullWidth">
        <Header />
        <Body />
        <Footer />
      </Container>
    )
  }
}

export default App;
