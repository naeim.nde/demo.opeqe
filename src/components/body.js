import React from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Tabs,
  Tab,
  Modal,
} from "react-bootstrap";

import Carousel from "./carousel";

import carousel_img01 from "../assets/images/carousel_img/01.jpg";
import carousel_img02 from "../assets/images/carousel_img/02.jpg";
import carousel_img03 from "../assets/images/carousel_img/03.jpg";
import carousel_img04 from "../assets/images/carousel_img/04.jpg";
import carousel_img05 from "../assets/images/carousel_img/05.jpg";
import carousel_img06 from "../assets/images/carousel_img/06.jpg";

class Body extends React.Component {
  state = {
    carouselItems0: [
      {
        image: carousel_img01,
        label: "code FREEDINE",
        title: "Caesar Salad",
        subtitle: {
          main: { name: "Salad", link: "/" },
          sub: { name: "AmericanMain", link: "/" },
          sub2: { name: "Appetizer", link: "/" },
        },
        tags: { time: "7-8 min", price: "29$", free: "Free Pickup" },
      },
      {
        image: carousel_img02,
        label: "code FREEDINE2222",
        title: "Roasted Chicken Salad",
        subtitle: {
          main: { name: "Salad", link: "/" },
          sub: { name: "AmericanMain", link: "/" },
          sub2: { name: "Appetizer", link: "/" },
        },
        tags: { time: "7-8 min", price: "29$", free: "Free Pickup" },
      },
    ],
    carouselItems1: [
      {
        image: carousel_img01,
        label: "code FREEDINE",
        title: "Caesar Salad",
        subtitle: {
          main: { name: "Salad", link: "/" },
          sub: { name: "AmericanMain", link: "/" },
          sub2: { name: "Appetizer", link: "/" },
        },
        tags: { time: "7-8 min", price: "29$", free: "Free Pickup" },
      },
      {
        image: carousel_img02,
        label: "code FREEDINE2222",
        title: "Roasted Chicken Salad",
        subtitle: {
          main: { name: "Salad", link: "/" },
          sub: { name: "AmericanMain", link: "/" },
          sub2: { name: "Appetizer", link: "/" },
        },
        tags: { time: "7-8 min", price: "29$", free: "Free Pickup" },
      },
      {
        image: carousel_img03,
        label: "",
        title: "Kale Salad",
        subtitle: {
          main: { name: "Salad", link: "/" },
          sub: { name: "AmericanMain", link: "/" },
          sub2: { name: "Appetizer", link: "/" },
        },
        tags: { time: "7-8 min", price: "29$", free: "Free Pickup" },
      },
      {
        image: carousel_img04,
        label: "",
        title: "Chopped Salad",
        subtitle: {
          main: { name: "Salad", link: "/" },
          sub: { name: "AmericanMain", link: "/" },
          sub2: { name: "Appetizer", link: "/" },
        },
        tags: { time: "7-8 min", price: "29$", free: "Free Pickup" },
      },
      {
        image: carousel_img05,
        label: "",
        title: "California Chicken Salad",
        subtitle: {
          main: { name: "Salad", link: "/" },
          sub: { name: "AmericanMain", link: "/" },
          sub2: { name: "Appetizer", link: "/" },
        },
        tags: { time: "7-8 min", price: "29$", free: "Free Pickup" },
      },
      {
        image: carousel_img06,
        label: "",
        title: "Greek Salad",
        subtitle: {
          main: { name: "Salad", link: "/" },
          sub: { name: "AmericanMain", link: "/" },
          sub2: { name: "Appetizer", link: "/" },
        },
        tags: { time: "7-8 min", price: "29$", free: "Free Pickup" },
      },
    ],
    translateX: 86,
    orderType: "pickup"
  };

  selectOrder = (type) => (e) => {
    console.log(type);
    
    this.setState((oldState) => {
      let newState = { ...oldState };
      if (type === "delivery"){
        newState.translateX = 0;
        newState.orderType = "delivery"
      } else {
        newState.translateX = 86;
        newState.orderType = "pickup"
      }
      return newState;
    });
  }

  render() {
    return (
      <Container fluid>
        <div style={{ paddingLeft: 40, paddingRight: 40, marginTop: 85}}>
          <Row>
            <Col md={12}>
              <Carousel
                title="Special Offers"
                carouselItems={this.state.carouselItems0}
              />
            </Col>
            <Col md={12}>
              <Carousel
                title="Salad"
                carouselItems={this.state.carouselItems1}
              />
            </Col>
            <Col md={12}>
              <Carousel
                title="Appetizer"
                carouselItems={this.state.carouselItems1}
              />
            </Col>
            <Col md={12}>
              <Carousel
                title="Main Course"
                carouselItems={this.state.carouselItems1}
              />
            </Col>
            <Col md={12}>
              <Carousel
                title="American"
                carouselItems={this.state.carouselItems1}
              />
            </Col>
            <Col md={12}>
              <div className="order_ready">
                <div className="title">
                  <h4>Ready to order?</h4>
                </div>
                <div className="subtitle">
                  <h6>
                    Browse our menu for dine-in, delivery or pickup and catering
                  </h6>
                </div>
                <div className="action_order">
                  <div className="option">
                    {this.state.orderType === "pickup" ?
                    <div className="option_description">
                      <div className="mode">ASAP Pickup</div>
                      <div className="sub">Beverly Hills - 1008 Elden Way</div>
                    </div>
                    : 
                    <div className="option_description">
                      <div className="mode">ASAP Delivery</div>
                      <div className="add_address">What's Your Address ?</div>
                    </div>
                    }
                  </div>
                  <div className="order_type">
                    <div className="order_change">change</div>
                    <div className="order_change_to">
                      <div className="order_delivery" onClick={this.selectOrder("delivery")}>Delivery</div>
                      <span className="seperator">or</span>
                      <div className="order_pickup" onClick={this.selectOrder("pickup")}>Pickup</div>
                      <div className="line" style={{transform: `translate(${this.state.translateX}px)`}}></div>
                    </div>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </Container>
    );
  }
}

export default Body;
