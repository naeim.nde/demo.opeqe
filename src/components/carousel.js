import React from "react";
import { Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronRight,
  faChevronLeft,
  faStopwatch,
} from "@fortawesome/free-solid-svg-icons";

class Carousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      carouselItems: props.carouselItems,
      itamCount: props.carouselItems.length,
      translateX: 0,
      showItem:
        window.innerWidth < 768
          ? 1
          : window.innerWidth > 768 && window.innerWidth < 992
          ? 2
          : 3,
      screenSize: window.innerWidth,
      translateXLine: 0,
    };
  }

  componentDidMount() {
    console.log(this.state);
  }

  nextAction = () => {
    console.log("next");
    this.setState((oldState) => {
      let newState = { ...oldState };
      if (newState.translateX < 0) {
        newState.translateX += 410 * this.state.showItem;
        newState.translateXLine -= 410 * this.state.showItem;
      }
      return newState;
    });
  };

  prevAction = () => {
    console.log("prev");
    this.setState((oldState) => {
      let newState = { ...oldState };
      let limit = this.state.itamCount / this.state.showItem;
      if (newState.translateX > -(410 * limit)) {
        console.log(325 * this.state.showItem);
        newState.translateX -= 410 * this.state.showItem;
        newState.translateXLine += 410 * this.state.showItem;
      }
      return newState;
    });
  };
  render() {
    return (
      <div className="carousel_container">
        <div className="carousel_title_container">
          <div className="carousel_title">{this.props.title}</div>
          <div className="carousel_title_line" style={{transform: `translate(${this.state.translateXLine}px`}}></div>
        </div>
        <div className="carousel_body_container">
          {this.state.itamCount >= this.state.showItem ? (
            <div className="carousel_body_action">
              <div className="next" onClick={this.nextAction}>
                <FontAwesomeIcon icon={faChevronRight} />
              </div>
              <div className="previous" onClick={this.prevAction}>
                <FontAwesomeIcon icon={faChevronLeft} />
              </div>
            </div>
          ) : (
            <div></div>
          )}
          <div
            className="carousel_body"
            style={{ transform: `translate(${this.state.translateX}px)` }}
          >
            {this.state.carouselItems.map((carouselItem, key) => {
              return (
                <div className="carousel_item" key={key}>
                  <div className="carousel_item_img">
                    <img src={carouselItem.image}></img>
                    <div className="carousel_item_label">
                      {carouselItem.label}
                    </div>
                  </div>
                  <div className="carousel_item_description">
                    <div className="carousel_item_title">
                      {carouselItem.title}
                    </div>
                    <div className="carousel_item_subtitle">
                      <a
                        className="main link"
                        href={carouselItem.subtitle.main.link}
                      >
                        {carouselItem.subtitle.main.name}
                      </a>
                      <a
                        className="sub link"
                        href={carouselItem.subtitle.sub.link}
                      >
                        {carouselItem.subtitle.sub.name}
                      </a>
                      <a
                        className="sub link"
                        href={carouselItem.subtitle.sub2.link}
                      >
                        {carouselItem.subtitle.sub2.name}
                      </a>
                    </div>
                    <div className="carousel_item_tags">
                      <div className="time">
                        <FontAwesomeIcon icon={faStopwatch} />
                        {carouselItem.tags.time}
                      </div>
                      <div className="price">{carouselItem.tags.price}</div>
                      <div className="free">{carouselItem.tags.free}</div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default Carousel;
