import React from "react";
import { Container, Row, Col, Form, Button, Tabs, Tab, Modal } from "react-bootstrap";

import Logo from "./logo";
import Menu from "./menu";

class Header extends React.Component {

  render() {
    return(
      <Container fluid className="header">
        <Row>
          <Col md={12}>
            <Row>
              <Col md={6}>
                <Logo />
              </Col>
              <Col md={6}>
                <Menu />
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default Header;
