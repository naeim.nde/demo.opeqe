import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faInstagram,
  faTwitter,
  faFacebookSquare,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";

class SocialMedia extends React.Component {
  render() {
    return (
      <div className="social_media">
        <a className="link social">
          <FontAwesomeIcon icon={faInstagram} />
        </a>
        <a className="link social">
          <FontAwesomeIcon icon={faTwitter} />
        </a>
        <a className="link social">
          <FontAwesomeIcon icon={faFacebookSquare} />
        </a>
        <a className="link social">
          <FontAwesomeIcon icon={faYoutube} />
        </a>
      </div>
    );
  }
}

export default SocialMedia;
