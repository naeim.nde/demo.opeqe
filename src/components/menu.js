import React from "react";

class Menu extends React.Component {
  render() {
    return (
      <div className="Menu_right">
        <a
          className="link menu"
          href="/"
        >
          Reservation
        </a>
        <a
          className="link menu"
          href="/"
        >
          Orders
        </a>
        <a
          className="link menu"
          href="/"
        >
          Locations
        </a>
        <div className="menu_action login_btn">
            <a href="/" className="link">Log In</a>
        </div>
        <div className="menu_action register_btn">
            <a href="/" className="link">Sign Up</a>
        </div>
        <div className="menu_action card_btn">
            <a href="/" className="link">
                <span>
                    <svg className="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit" fill="rgb(2, 103, 100)" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M17.21 9l-4.38-6.56c-.19-.28-.51-.42-.83-.42-.32 0-.64.14-.83.43L6.79 9H2c-.55 0-1 .45-1 1 0 .09.01.18.04.27l2.54 9.27c.23.84 1 1.46 1.92 1.46h13c.92 0 1.69-.62 1.93-1.46l2.54-9.27L23 10c0-.55-.45-1-1-1h-4.79zM9 9l3-4.4L15 9H9zm3 8c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"></path></svg>
                </span>
            </a>
        </div>
      </div>
    );
  }
}

export default Menu;
